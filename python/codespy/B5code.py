from PIL import Image


def rouge_paire(nom_fichier,nom_fichier_sortie):
    i = Image.open(nom_fichier)
    sortie = i.copy()
    for y in range(i.size[1]):
        for x in range(i.size[0]):
            (rouge, vert, bleu) = i.getpixel((x, y))
            if rouge %2 == 1:
                rouge -= 1
            # Si la valeur rouge est impaire, on lui retire 1
            sortie.putpixel((x,y),(rouge, vert, bleu))
    sortie.save(nom_fichier_sortie)
rouge_paire("./python/imagespy/hall-mod_0.bmp", "./python/imagespy/Imageout_steg_0.bmp")


def cacher(i, b):
    return i-(i%2)+b


def trouver(i):
    return i%2


def cacher_image(image_origine,image_a_cacher, nom_fichier_sortie):
    img_orig = Image.open(image_origine)
    img_cachee = Image.open(image_a_cacher)
    sortie = img_orig.copy()
    for y in range(img_cachee.size[1]):
        for x in range(img_cachee.size[0]):
            # On parcourt l'image cachée
            c_cachee = img_cachee.getpixel((x,y)) # On crée une valeur "couleurs de l'image cachée"
            c_orig = img_orig.getpixel((x,y)) # On crée une valeur "couleurs de l'image d'origine"
            total = (c_cachee[0] * c_cachee[0] + c_cachee[1] * c_cachee[1] + c_cachee[2] * c_cachee[2])
            if total > 255 * 255 * 3/2: # On vérifie la couleur du pixel caché (noir ou blanc)
                sortie.putpixel((x,y),(cacher(c_orig[0], 1), c_orig[1], c_orig[2]))
            else:
                sortie.putpixel((x,y),(cacher(c_orig[0], 0), c_orig[1], c_orig[2]))
    sortie.save(nom_fichier_sortie)
cacher_image("./python/imagespy/Imageout_steg_0.bmp", "./python/imagespy/Imageout3.bmp", "./python/imagespy/Imageout_steg_1.bmp")


def trouver_image(image_origine, nom_fichier_sortie):
    img_orig = Image.open(image_origine)
    sortie = img_orig.copy()
    for y in range(img_orig.size[1]):
        for x in range(img_orig.size[0]):
            # On parcourt l'image d'origine
            couleur = img_orig.getpixel((x, y)) # On crée une valeur "couleurs de l'image d'origine"
            if trouver(couleur[0]) == 0:
                sortie.putpixel((x,y),(0,0,0))
                # Si la couleur rouge a comme valeur 0, on ajoute à l'image de sortie du noir
            elif trouver(couleur[0]) == 1:
                sortie.putpixel((x,y),(255, 255, 255))
                # Sinon si la couleur rouge a comme valeur 1, on ajoute à l'image de sortie du blanc
    sortie.save(nom_fichier_sortie)
trouver_image("./python/imagespy/Imageout_steg_1.bmp", "./python/imagespy/Imagecachee.bmp")
        
