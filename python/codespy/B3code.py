from PIL import Image

i = Image.open("./images/ImageExemple.bmp")
sortie = i.copy()
# On crée une copie de l'image
for y in range(i.size[1]):
    for x in range(i.size[0]):
        # On parcourt l'image
        (rouge, vert, bleu) = i.getpixel((x, y))
        # La valeur c est en fait un tuple contenant les couleurs rouge, vert et bleu
        gris = (rouge+vert+bleu)//3
        # On définit une variable "gris" qui applique donc la formule donnée dans l'énoncé 
        sortie.putpixel((x,y),(gris, gris, gris))
        # On utilise un tuple n'utilisant que la variable "gris" à chaque couleur
# On sauvegarde l'image de sorte à la renommer
sortie.save("./python/imagespy/Imageout2.bmp")

