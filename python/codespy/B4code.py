from PIL import Image

i = Image.open("./images/ImageExemple.bmp")
sortie = i.copy()
# On crée une copie de l'image
for y in range(i.size[1]):
    for x in range(i.size[0]):
        # On parcourt l'image
        (rouge, vert, bleu) = i.getpixel((x, y))
        total = (rouge * rouge + vert * vert + bleu * bleu)
        # Création d'une valeur "total" permettant de savoir si l'on choisit le blanc ou le noir total
        if total > 255 * 255 * 3/2:
            sortie.putpixel((x,y),(255, 255, 255))
            # Si le total est supérieur à la formule de l'énoncé, on choisit le blanc total
        else:
            sortie.putpixel((x,y),(0,0,0))
            # Sinon, on choisit le noir total
        
# On sauvegarde l'image de sorte à la renommer
sortie.save("./python/imagespy/Imageout3.bmp")

