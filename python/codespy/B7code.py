def chiffre_de_vernam(message_chiffre, clef):
    if len(message_chiffre) < len(clef):
        return None
    # On vérifie que la clé aie au moins la même taille que le texte
    alphabet = {'a':0, 'b':1,'c':2,'d':3,'e':4,'f':5,'g':6, 'h':7,'i':8,'j':9,'k':10,'l':11,'m':12,'n':13,'o':14,'p':15,'q':16,'r':17,'s':18,'t':19,'u':20,'v':21,'w':22,'x':23,'y':24,'z':25}
    # On vérifie que le texte et la clé aient bien le même nombre de lettres
    position_lettre = 0 # Initialisation de la position d'une lettre qui sera ajoutée dans la liste
    liste_positions = [] # Création d'une liste permettant de retrouver la clé d'une valeur dans le dictionnaire
    message_crypte = '' # On définit une variable qui contiendra le message crypté
    for i in range(len(message_chiffre)): # On parcourt le message chiffré
        for j in range(i,len(clef)): # On parcourt la clef de sorte à ce que les éléments aux mêmes indices soient comparés
            position_lettre = (alphabet[message_chiffre[i]] + alphabet[clef[j]]) % 26
            liste_positions.append(position_lettre) # On additionne les deux valeurs de l'alphabet pour l'élément du texte et de la clé et on les met modulo 26 pour obtenir le reste
            break # On arrête la boucle pour éviter des comparaisons en trop après que la taille du texte et de la clé sont parcourues 
    for position in liste_positions: # On parcourt la liste
        for lettre_alphabet, position_alphabet in alphabet.items(): # On parcourt le dictionnaire
            if position == position_alphabet: # Si la position dans la liste correspond à une parcourue dans l'alphabet
                message_crypte += lettre_alphabet # On ajoute au message crypté la clé en question pour en obtenir des lettres
    return message_crypte



def decrypter_chiffre_de_vernam(message_crypte, clef):
    if len(message_crypte) < len(clef):
        return None
    # On vérifie que la clé aie au moins la même taille que le texte
    alphabet = {'a':0, 'b':1,'c':2,'d':3,'e':4,'f':5,'g':6, 'h':7,'i':8,'j':9,'k':10,'l':11,'m':12,'n':13,'o':14,'p':15,'q':16,'r':17,'s':18,'t':19,'u':20,'v':21,'w':22,'x':23,'y':24,'z':25}
    # On vérifie que le texte et la clé aient bien le même nombre de lettres
    position_lettre = 0 # Initialisation de la position d'une lettre qui sera ajoutée dans la liste
    liste_positions = [] # Création d'une liste permettant de retrouver la clé d'une valeur dans le dictionnaire
    message_decrypte = '' # On définit une variable qui contiendra le message crypté
    for i in range(len(message_crypte)): # On parcourt le message chiffré
        for j in range(i,len(clef)): # On parcourt la clef de sorte à ce que les éléments aux mêmes indices soient comparés
            position_lettre = (alphabet[message_crypte[i]] - alphabet[clef[j]]) % 26
            liste_positions.append(position_lettre) # On soustrait les deux valeurs de l'alphabet pour l'élément du texte et de la clé et on les met modulo 26 pour obtenir le reste
            break # On arrête la boucle pour éviter des comparaisons en trop après que la taille du texte et de la clé sont parcourues 
    for position in liste_positions: # On parcourt la liste
        for lettre_alphabet, position_alphabet in alphabet.items(): # On parcourt le dictionnaire
            if position == position_alphabet: # Si la position dans la liste correspond à une parcourue dans l'alphabet
                message_decrypte += lettre_alphabet # On ajoute au message crypté la clé en question pour en obtenir des lettres
    return message_decrypte

#print(decrypter_chiffre_de_vernam("rofqmfurfctx", "pateencroute"))
