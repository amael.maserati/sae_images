from PIL import Image

def rouge_paire(nom_fichier,nom_fichier_sortie):
    i = Image.open(nom_fichier)
    sortie = i.copy()
    for y in range(i.size[1]):
        for x in range(i.size[0]):
            (rouge, vert, bleu) = i.getpixel((x, y))
            if rouge %2 == 1:
                rouge -= 1
            # Si la valeur rouge est impaire, on lui retire 1
            sortie.putpixel((x,y),(rouge, vert, bleu))
    sortie.save(nom_fichier_sortie)
rouge_paire("./python/imagespy/hall-mod_0.bmp", "./python/imagespy/ImageB6.bmp")

def cacher(i, b):
    return i-(i%2)+b


def trouver(i):
    return i%2

def conversion_binaire(texte):
    liste_decimale = [] # On crée une liste de valeurs décimales
    for lettre in texte:
        liste_decimale.append(ord(lettre)) # On convertit chaque lettre du texte en ascii pour l'ajouter dans la liste en décimal
    liste_binaire = [] # On crée une liste de nombres binaires
    nombre_bin = 0 # On crée une valeur pour les nomnbres binaires, pour l'instant à 0 
    nombre_str = '' # On crée un texte pour que chaque nombre binaire soit transformé par la suite en chaîne de caractères
    for nombre in liste_decimale:
        nombre_bin = bin(nombre)[2:] 
        # On convertit chaque élément de la liste en binaire en partant de l'indice 2 car la commande 'bin' ajoute '0b' au début du nombre
        nombre_str = "0" + str(nombre_bin) # On rajoute un 0 au nombre pour le mettre sur 8 bits
        for nb in range(len(nombre_str)):
            liste_binaire.append(nombre_str[nb]) # On ajoute chaque nombre binaire en str dans la liste de nombres binaires
    return liste_binaire

def cacher_texte(image_origine,texte, nom_fichier_sortie):
    img_orig = Image.open(image_origine)
    sortie = img_orig.copy()
    texte_binaire = conversion_binaire(texte) # On appelle la fonction pour convertir un texte en tableau de nombres binaires
    ind_bin = 0 # On crée un indice pour chaque élément d'un nombre binaire
    for y in range(img_orig.size[1]):
        for x in range(img_orig.size[0]):
            c_orig = img_orig.getpixel((x,y)) # On crée une valeur "couleurs de l'image d'origine"
            if ind_bin < len(texte_binaire): # Pour éviter un 'out of range', on met cette condition 
                if texte_binaire[ind_bin] == '0': # Si la valeur cachée en binaire est 0
                    sortie.putpixel((x,y),(cacher(c_orig[0], 0), c_orig[1], c_orig[2]))
                else: # Si la valeur cachée en binaire est 1
                    sortie.putpixel((x,y),(cacher(c_orig[0], 1), c_orig[1], c_orig[2]))
                ind_bin += 1 # On incrémente cette valeur à chaque tour de boucle
    sortie.save(nom_fichier_sortie)
cacher_texte("./python/imagespy/ImageB6.bmp", "jaieudumalmaisjaireussilaquestion", "./python/imagespy/Imageout_steg_1.bmp")

def convertir_binaire_ascii(liste_binaire):
    liste_nb_binaire_temp = [] # On définit une liste temporaire contenant un nombre binaire de 8 bits dont chaque bit est séparé
    liste_decimale = [] # On définit une liste qui contiendra les nombres binaires en base 10
    liste_ascii = [] # On définit une liste qui servira de convertisseur des nombres décimaux en ascii
    binaire_a_decimal = 0 # On initialise à 0 un nombre qui servira de conversion entre le binaire et le decimal
    puissance = 0 # On fait de même pour une puissance par rapport à la conversion
    for nb in liste_binaire: # On parcourt la liste de nombres binaires
        liste_nb_binaire_temp.append(nb) # On y ajoute des 0 et des 1
        if len(liste_nb_binaire_temp) == 8: # Si la taille de la liste temporaire contient les 8 bits d'un nombre
            for i in range(len(liste_nb_binaire_temp)-1, -1, -1): # On parcourt à l'envers cette liste temporaire
                binaire_a_decimal += liste_nb_binaire_temp[i]*(2**puissance) 
                # On ajoute à la variable "binaire_a_decimal" les nombres en base 2 convertis en base 10
                puissance += 1 # On incrémente la puissance de 1
                if i == 0: # Si toute la liste a été parcourue
                    liste_decimale.append(binaire_a_decimal) # On ajoute à la liste décimale le nombre en base 10
                    binaire_a_decimal = 0 # On réinitialise le nombre binaire_a_decimal
                    puissance = 0 # De même pour la puissance
                    liste_nb_binaire_temp = [] # Et la liste binaire temporaire redevient vide
    for nb in liste_decimale: # Lorsque la procédure est terminée on parcourt la liste de nombres décimaux
        liste_ascii.append(chr(nb)) # On les ajoutes dans la liste de nombres ascii en les convertissant en ascii grâce à la commande "chr"
    return liste_ascii
   
def trouver_texte(image):
    img = Image.open(image)
    liste_binaire = [] # On crée une liste de nombres binaires
    liste_ascii = [] # On crée une liste de nombres en ascii
    for y in range(img.size[1]):
        for x in range(img.size[0]):
            # On parcourt l'image d'origine
            couleur = img.getpixel((x, y)) # On crée une valeur "couleurs de l'image"
            if trouver(couleur[0]) == 0:
                liste_binaire.append(0)
            # Si la valeur trouvée pour la couleur rouge est un 0, on ajoute 0 dans la liste binaire
            elif trouver(couleur[0]) == 1:
                liste_binaire.append(1)
            # Si la valeur trouvée pour la couleur rouge est un 1, on ajoute 0 dans la liste binaire
    for elem in convertir_binaire_ascii(liste_binaire): # On parcourt la liste binaire tout en appelant la fonction  "convertir_binaire_ascii"
        if elem != '\x00': # Si l'élément parcourut ne correspond pas à '\x00'
            liste_ascii.append(elem) # On l'ajoute dans la liste en ascii
    return ''.join(liste_ascii) # On retourne un texte à partir des valeurs dans la liste en ascii
 
#print("Le texte caché est :", trouver_texte("./python/imagespy/Imageout_steg_1.bmp"))