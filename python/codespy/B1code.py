from PIL import Image

i = Image.open("./python/imagespy/Imagetest.bmp")
sortie = i.copy()
# On crée une copie de l'image
for y in range(i.size[1]):
    for x in range(i.size[0]):
        # On parcourt l'image
        c = i.getpixel((x, y))
        # On inverse les valeurs de x et de y pour faire la tranposée
        sortie.putpixel((y,x),c)
# On sauvegarde l'image de sorte à la renommer
sortie.save("./python/imagespy/Imageout0.bmp")

