<h1 align='center'>SAE Image</h1>

# Partie A

## A.0 :

Les premières valeurs présentes à partir de l'adresse 00 et étant sur 2 octets correspondent au type de l'image analysée. En utilisant le tableau ASCII, nous remarquons que 42 signifie B et que 4D signifie M. Nous pouvons donc en déduire que le type de l'image est BMP, donc Bitmap.

Ensuite, à l'adresse 02 se trouve la taille de l'image sur 4 octets et comme le BMP se lit en Little Endian, les valeurs concernant cette dernière sont : 00 0C 73 99. Par le biais d'un calcul, cette valeur qui est en hexadécimal vaut 816025 en décimal. La taille du fichier est donc de 816025 octets.

Les 4 octets suivants qui sont de 00 00 00 00, à l'adresse 06, correspondent à la partie réservée.

Par la suite, à l'adresse 08 se trouvent sur 4 octets les valeurs 00 00 00 1A qui sont l'adresse de la zone de définition de l'image. 

La taille de l'entête de l'image est située juste après l'adresse de la zone de définition de l'image, en 0E, et nous y trouvons sur 4 octets les valeurs suivantes : 00 00 00 0C. En décimal, cela vaut 12. La taille de cet entête est alors de 12 octets.

A la base, une image en Bitmap a une largeur et une hauteur s'étendant sur 4 octets. Or, celle qui est étudiée est en Bitmap 2.0, ayant la particularité de n'étendre ces derniers que sur 2 octets.

Nous avons à l'adresse 12 sur 2 octets la largeur de l'image qui est en hexadécimal de 02 80, et cela correspond à 640 en décimal, donc de 640px.

A l'adresse 14 se trouvent les valeurs 01 A9 qui correspondent à la hauteur de l'image et qui valent 425px.

Les deux octets suivants définissent le nombre de plans, dont les valeurs sont : 00 01. Cela vaut 1 en décimal.

A la suite se trouve la profondeur des couleurs qui est en hexadécimal de 00 18, qui est donc de 24 en décimal. Il faut donc 24 bits, soit 24/8 = 3 octets pour représenter un pixel.

Concentrons nous maintenant sur le problème avec la commande display. En effet, cette dernière permet d'afficher l'image, mais nous pouvons constater une erreur relevée par le terminal :

![message d'erreur dans le terminal](./images/A0pbterminal.png)

Cela signifie donc que la longueur et la taille du fichier ne correspondent pas. Nous avons un peu plus tôt dit que la taille du fichier était de 816025 octets. Regardons quelle est sa taille avec le terminal :

![visualisation de la taille de l'image dans le terminal](./images/A0terminaltaille.png)

La taille ci-présente est de 816026 octets, ce qui fait qu'il manque un octet dans okteta, alors il faut réparer cela. S'il manque un octet, cela signifique qu'il faut ajouter 1 à la taille du fichier en hexadécimal. Cela fait alors 00 0C 73 99 + 1 = 00 0C 73 9A. Modifions le dans le logiciel en question.

![modification de la taille du fichier dans okteta](./images/A0modiftaille.png)

Effectuons à nouveau la commande display dans le terminal pour voir s'il y a à nouveau un message d'erreur : 

![vérification d'une potentielle erreur concernant l'image avec la commande display](./images/A0verifconf.png)

Aucun message en particulier ne s'affiche, le problème est alors réglé.

## A.1 :

Après avoir suivi toutes les étapes indiquées dans la consigne de la SAE, nous voyons grâce à la commande ls -l la taille de l'image :

![observation de la taille de l'image à créer](./images/A1tailleimage.png)

Pour les couleurs, en héxadécimal, la couleur blanche correspond à FF FF FF et la couleur rouge à FF 00 00, qu'il a fallu entrer en Little Endian.
La taille étant de 74 octets, il faut la convertir en hexadécimal pour éviter un problème similaire à la question **A.0**, et grâce à un calcul nous savons que cela vaut 4A en base 16. Il faut ensuite l'implémenter dans okteta. Voici quel est le résultat final grâce aux valeurs insérées dans okteta, et ce que cela affiche :

![okteta de l'image 0](./images/A1okteta.png)

![affichage de l'image 0](./images/A1display.png)

## A.2 :

Voici à quoi ressemble la partie okteta de l'image et l'image en elle-même : 

![okteta de l'image test](./images/A2okteta.png)

![affichage de l'image test](./images/A2display.png)

Concernant les couleurs, le site internet qui nous a été donné nous indiquait les valeurs en base 10. Il a donc été nécessaire de les convertir en base 16, et cela permet de les implémenter dans okteta. Le cyan correspond à 00 FF FF, magenta à FF 00 FF et le bleu céruléen à 0F 9D E8.

## A.3 :

En exécutant la commande de conversion de l'image de la question **A.1**, nous avons une image nommée "Image1.bmp" qui a un okteta se présentant de cette façon : 

![okteta de l'image 1](./images/A3okteta.png)

1. A l'adresse 001C se trouvent sur 2 octets les chiffres 00 18, valant en base 10 le nombre 24, ce qui veut dire qu'il faut 24 bits pour coder un pixel.
2. A l'adresse 0022, nous voyons le nombre 30 qui vaut en décimal 48, donc la taille des données pixels est de 48 octets.
3. A l'adresse 001E se trouvent sur 4 octets les chiffres 00 00 00 00, ce qui signifie que pour cette image, aucune compression n'a été utilisée.
4. Le codage des pixels n'a pas changé car il fallait le même nombre de bits pour en coder un avec **Image0.bmp**.

## A.4 :

Après avoir exécuté la commande donnée dans la consigne, nous obtenons notre **Image2** avec un okteta de cette allure : 

![okteta de l'image 2](./images/A4okteta.png)

1. A l'adresse 001C se trouvent sur 2 octets les chiffres 00 01. Cela signifique qu'il y a 1 bit par pixel pour cette image.

2. La taille des données pixels se trouvant à l'adresse 0022, nous apercevons donc sur 2 octets les valeurs 00 10, correspondant en décimal à 16 en décimal. Celle-ci est de 16 octets.

3. A l'adresse 001E, sont présentes sur 4 octets les valeurs 00 00 00 00, ce qui veut dire qu'il n'y a pas de compression pour cette image.

4. Comme chaque couleur de palette est codée sur 3 octets BVR et un octet réservé, les couleurs de la palette sont donc codées sur BB VV RR 00.

5. A l'adresse 002E, on remarque qu'il y a 2 couleurs dans la palette grâce aux valeurs 00 02, qui donne 2 en décimal.

6. Le codage des pixels a changé étant donné qu'à la base un pixel était codé sur 24 bits avant la conversion et maintenant il est codé sur 1 bit.

7. La couleur bleu correspond en hexadécimal à 00 00 FF, il va donc falloir appliquer un changement de la couleur rouge sur okteta de manière à ce que ce soit lu en Little Endian :

![okteta de l'image changée en bleu](./images/A4oktetableu.png)

Voici ce qui est donc affiché : 

![affichage de l'image bleue](./images/A4affichagebleu.png)

8. Pour inverser les damiers blancs et bleus, il suffit d'inverser les couleurs dans okteta, qui ressemble donc à cela :

![okteta de l'image inversée](./images/A4oktetainv.png)

L'image suivante est affichée :

![affichage de l'image bleue inversée](./images/A4imageinv.png)

9. Voici le passage de l'image en index de couleurs avec okteta : 

![conversion de l'image en index de couleur](./images/A4conversionindex.png)

Cela représente ceci sur okteta :

![okteta de l'image en index de couleur](./images/A4oktetaindex.png)

Voyons maintenant comment changer les couleurs de l'image de sorte à arriver à ce qu'il nous est demandé dans la consigne. Tout d'abord, nous savons grâce à wikipedia que chaque ligne doit être écrite sur 4 octets, ce qui signifie que 00 00 00 50 correspond à une ligne. 50 s'écrit en binaire 01010000, et si nous prenons en compte le chiffre 5 qui s'écrit 0101, on comprend donc que 0 correspond à la couleur rouge et 1 à la couleur blanche. Il faut alors modifier cette valeur de sorte à obtenir 1111 0000, ce qui correspond à FO. Cela permettra donc d'avoir une ligne toute blanche. Ensuite, pour obtenir des lignes toutes rouges, les valeurs 00 le permettent car en binaire cela fait 0000 0000, et les 4 premiers octets n'ayant que des 0, le 0 étant associé à la couleur rouge, la ligne va se faire comme il est souhaité. Nous changeons donc AO et 50 par 00. Pour finir, la ligne du haut doit rester telle qu'elle est car A0 correspond à 1010 0000 en binaire, et donc A étant de 1010 construit bien l'image de sorte à ce qu'il y ait la couleur blanche suivie de la rouge, et de nouveau le même procédure.

Voici à quoi ressemblent okteta et **Image3.bmp** après les modifications : 

![okteta de l'image 3 changée](./images/A4oktetaimage3.png)

![affichage de l'image 3](./images/A4displayimage3.png)

10. Voici la conversion de l'image : 

![conversion de l'image exemple](./images/A4q10.png)

Nous avons cela sur okteta : 

![okteta image exemple convertie](./images/A4q10index.png)

11. A l'adresse 002E, on peut trouver le nombre de couleurs qu'il y a dans la palette. Etant donné qu'à cette adresse le nombre est en hexadécimal de 00 10, cela signifie qu'il y a 16 couleurs dans la palette.

12. Grâce aux tests effectués sur le site internet des couleurs fourni, nous remarquons que les valeurs FE FE FD correspondent à la couleur à dominante "Blanc", et cela se trouve à l'adresse 0066.

13. Le tableau de pixels commence à l'adresse 0075.

14. Pour mettre des pixels tout en bas à gauche en bleu, il faut aller au tableau des pixels et changer les premières valeurs par FF 00 00 00 comme les couleurs sont codés en BB VV RR 00, donc la ligne changée sur okteta se présente sous cette forme :

![okteta de l'image exemple modifiée](./images/A4oktetamodif.png)

Et voici ce que cela affiche :

![affichage de l'image exemple modifiée](./images/A4gimp.png)

15. Si l'on diminue le nombre de couleurs dans la palette, d'un point de vue visuel, l'image est clairement moins esthétique car les couleurs ne sont pas très belles, et les écritures semblent moins soignées. Voici le résultat :

![point de vue visuel de l'image avec 4 couleurs](./images/A4q15visuel.png)

Voici son okteta :

![okteta de cette image](./images/A4q15okteta.png)

Nous pouvons constater qu'au niveau de l'hexadécimal, les valeurs sont grandement restreintes car il n'y a quasiment que des 1 de partout, et un peu plus loin se trouvent aussi des 2 et des 3. Les couleurs qui étaient auparavant de CC CC CC passent donc à 11 11 11.

## A.5 : 

2. Nous avons besoin du complément à 2 pour écrire -4 dans okteta. En effet, sur 4 octets, si l'on prend 4 qui est de 00 00 00 04, en l'inversant, cela donne FF FF FF FB. On y ajoute ensuite 1, ce qui fait : FF FF FF FB = 1 = FF FF FF FC. On l'implémente donc dans okteta : 

![okteta changement de la hauteur de l'image à -4 pixels](./images/A5okteta1.png)

Voici à quoi ressemble donc l'image :

![affichage de l'image après le changement de valeur](./images/A5display1.png)

Nous remarquons donc que les couleurs de l'image ont été inversées.

3. Il faut faire la même chose pour **ImageExempleIndexBMP3_16.bmp** si nous voulons obtenir le résultat demandé. A l'adresse 0016, nous voyons que la hauteur de l'image est de 00 00 01 A9, ce qui donne si nous inversons ces valeurs FF FF FE 56, et en complément à 2 : FF FF FE 56 + 1 = FF FF FF 57. Implémentons cela dans okteta et voyons ce qui est affiché ensuite :

![okteta de l'image de l'IUT avec le changement](./images/A5okteta2.png)

![affichage de l'image de l'IUT après le changement](./images/A5display2.png)

L'image obtenue a donc ses couleurs inversées.

## A.6 :

Après avoir comprimé les fichiers, voici à peu près à quoi ressemble **Image4.bmp** dans okteta :

![okteta de l'image 4](./images/A6okteta.png)

1. Nous voyons à l'adresse 0002 les valeurs 00 00 04 60, qui font en décimal 1120. Le poids de l'image est donc de 1120 octets. Le fichier est donc bien plus lourd que l'**Image1.bmp** qui était de 74 octets. A l'adresse 2E nous voyons avec les valeurs 00 00 01 00, signifiant qu'il y a 256 couleurs. Or, il y a pleins de mémoires allouées à des couleurs inutiles, d'où tous les 0 présents à la suite, ce qui fait que le fichier est plus lourd.

2. A l'adresse 0A se trouvent sur 4 octets les valeurs 00 00 04 36, ce qui veut dire que l'adresse des pixels est située en 436.

![okteta d'où commencent les pixels](./images/A6oktetapixels.png)

3. Les pixels sont codés sur 2 octets, le premier signifie le nombre de couleurs identiques se suivant, donc à l'adresse 436 avec 00 01 cela représente donc 00 pour la couleur rouge et 01 car un seul pixel rouge est présent d'affilée. Par la suite c'est la couleur blanche qui est représentée sur 2 octets avec 01 01, 01 signifiant donc au départ la couleur blanche. Nous comprenons donc qu'il y a un quadrillage rouge/blanc. A la fin d'une ligne, nous retrouvons les valeurs 00 00 pour signifier que cela ne correspond pas à un pixel mais plutôt à la fin d'une ligne étant donné qu'à l'adresse 43E et 44E la dernière valeur est un 0. Pour finir, à l'adresse 45E se trouvent les valeurs 01 00, le 1 marquant la fin de la bitmap.

## A.7 : 

1. Voici à quoi ressemble l'okteta de **Image5.bmp** : 

![okteta image 5](./images/A7okteta.png)

Nous voyons à l'adresse 0002 en hexadécimal les valeurs 04 4E, qui sont équivalentes à 1102 en décimal. L'image a donc un poids de 1102 octets, ce qui fait qu'elle est moins lourde qu'**Image4.bmp**. Nous remarquons à l'adresse 22 que la taille des données de l'image est de 24 octets car la valeur 18 en héxadécimal est présente, tandis que celle de l'**Image4.bmp** était de 2A, donc de 42 octets. De plus, regardons comment sont les pixels de cette image :

![okteta des pixels de l'image 5](./images/A7oktetapixels.png)

Nous pouvons voir qu'il y a une disposition différente des couleurs, ce qui fait que pour cette image plus de pixels de la même couleurs sont alignés. De ce fait, l'image 5 est donc moins lourde que l'image 4.

2. A l'adresse 436 sur 2 octets sont présentes les valeurs 01 01, ce qui signifie qu'il y a un pixel de couleur blanche. Par contre, étant donné que la hauteur de l'image est inversée, nous devons effectivement lire l'image de gauche à droite mais en partant du haut. Ensuite sont présentes les valeurs 00 01, ce qui signifie qu'il y a un pixel de couleur rouge, et cela s'alterne donc jusqu'à la fin de la ligne marquée par un 0 à l'adresse 43E sur 2 octets. Ensuite, à l'adresse 440 sont présents sur 2 octets les valeurs 00 04, ce qui veut dire que les 4 pixels se suivant, donc sur la même ligne, sont rouges. Ensuite, la fin de la ligne est marquée par les valeurs 00 00 sur 2 octets à l'adresse 442, puis la ligne suivante est identique. Pour finir, la dernière ligne correspond donc à 4 pixels de couleur blanche, et la bitmap se termine à l'adresse 44C grâce aux valeurs 01 00.

## A.8 : 

Voici à quoi ressemble l'okteta de **Image6.bmp** et ce que cela représente :

![okteta de l'image 6](./images/A8okteta.png)

![affichage de l'image 6](./images/A8display.png)

## A.9 : 

Nous ajoutons dans la palette de couleurs le vert et le bleu, le vert est 00 FF 00 et le bleu 00 00 FF :

![okteta de la palette de couleurs de l'image 7](./images/A9oktetacouleurs.png)

La couleur verte étant située en troisième dans la palette de couleurs, cela signifie qu'elle correspond à 02 dans les pixels et le bleu à 03 : 

![okteta des pixels de l'image 7](./images/A9oktetapixels.png)

Voici ce qu'elle affiche :

![[affichage de l'image 7](./images/A9display.png)

## A.10 : 

Voici à quoi ressemble **Image8.bmp**** :

![okteta de l'image 8](./images/A10oktetaimage8.png)

![affichage de l'image 8](./images/A10displayimg8.png)

Concentrons-nous maintenant sur **Image9.bmp** :

Ce qu'il a fallu faire était de supprimer à la main tous les 00 00 00 00 qui signifient la couleur noire. Ensuite, le nombre de couleurs de la palette est passé de 01 00 à 00 04 car il n'y a que 4 couleurs, ce qui fait que le nombre de couleurs importantes est à son tour passé à 00 04. De plus, l'adresse de début de pixels a été changé car ils commencent en 0046, donc il a fallu modifier les valeurs en 0010 sur 2 octets pour les passer à 00 46. Pour finir, la taille du fichier étant de 106 octets, il à son tour la changer en hexadécimal, qui fait 2A.

![taille de l'image 9](./images/A10tailleimage9.png)

Voici à quoi ressemble l'okteta d'**Image9.bmp** et ce qu'elle affiche : 

![okteta de l'image 9](./images/A10oktetaimage9.png)

![affichage de l'image 9](./images/A10displayimage9.png)

# Partie B

## B.1 :

Voici le code que j'ai effectué pour pouvoir transposer *Imagetest.bmp* : 

![code d'Imagetest.bmp transposée](./python/imagespy/B1code.png)

Voici ce que cela affiche : 

![affichage de l'image transposée](./python/imagespy/B1display.png)

## B.2 :

Voici le code effectué pour inverser une image dans un miroir : 

![code pour l'image sous forme de miroir](./python/imagespy/B2code.png)

Voici ce que cela affiche : 

![affichage de l'image inversée](./python/imagespy/B2display.png)

## B.3 : 

Voici le code pour mettre l'image de l'IUT entièrement grise :

![code de l'image de l'IUT en gris](./python/imagespy/B3code.png)

Voici ce que cela affiche : 

![affichage de l'image de l'IUT grise](./python/imagespy/B3display.png)

## B.4 : 

Voici le code permettant de mettre l'image de l'IUT en noir et blanc :

![code de l'image de l'IUT en noir et blanc](./python/imagespy/B4code.png)

Voici ce qu'elle affiche : 

![affichage de l'image de l'IUT en noir et blanc](./python/imagespy/B4display.png)

## B.5 :

Voici le code qui permet de passer toutes les valeurs rouges de chaque pixel à une valeur paire :

![code pour passer toutes les valeurs rouges de chaque pixel à une valeur paire](./python/imagespy/B5coderp.png)

Voici la preuve que cela fonctionne : 

![affichage des valeurs pour chaque couleur](./python/imagespy/B5affichagevaleursrouges.png)

L'image n'a donc subi aucun changement :

![affichage de l'image avec les valeurs rouges paires](./python/imagespy/B5imagesanschangement.png)

On implémente ensuite les fonctions **cacher** et **trouver** : 

![fonctions python cacher et trouver](./python/imagespy/B5cachertrouver.png)

Voici le code qui permet de cacher une image :

![fonction python pour cacher une image](./python/imagespy/B5codecacherimage.png)

Nous voyons donc que l'image est bien cachée : 

![affichage de l'image contenant celle cachée](./python/imagespy/B5cacheeinvisible.png)

Voici le code qui permet de trouver l'image cachée :

![fonction python qui trouve l'image cachée](./python/imagespy/B5codetrouverimg.png)

Voici quelle était cette dernière : 

![affichage de l'image cachée](./python/imagespy/B5imgcachee.png)

## B.6 :

Pour cacher un texte dans une image, voici ma procédure : 

1. J'ai réutilisé la fonction de la question **B5** pour faire en sorte que la couleur rouge de chaque pixel d'une image soit rouge pair.

2. J'ai fait une fonction permettant de convertir un texte en binaire de sorte à ce que chaque lettre soit mise sous forme de nombre binaire à 8 bits, par exemple : "00111111". Voici comment j'ai effectué cela : 

![code python de converison d'un texte en binaire](./python/imagespy/B6codeconversiontxtbinaire.png)

3. J'ai réutilisé le code **cacher_image** de la B5 mais avec des modifications car nous avons besoin de cacher un texte cette fois-ci, donc j'ai nécessité une liste en binaire. Voici à quoi ressemble mon code : 

![code python de cacher texte](./python/imagespy/B6codecachertexte.png)

4. J'ai créé une fonction qui permet de convertir à partir d'une liste de nombres binaires sous la forme [0, 1, 1, 0, 1,...] en ascii. La fonction se présente sous la sorte suivante : 

![code python convertisseur de nombres binaires en ascii](./python/imagespy/B6codeconvertirbinaireascii.png)

5. Je réutilise la fonction **trouver_image** en cherchant cette fois-ci à trouver un texte. Mon programme ressemble à cela : 

![code python de trouver texte](./python/imagespy/B6codetrouvertexte.png)

Voici quel était le texte caché dans l'image : 

![affichage du texte caché dans l'image](./python/imagespy/B6textecache.png)

## B.7 : 

Le chiffre de Vernam est donc considéré comme incassable car l'être humain n'a pas la capacité de connaître la clé de chiffrement, et le message ne peut alors pas être déchiffré à son tour. Il est obligé de la créer lui-même. En fait, si nous prenons une clé de la même taille que le message crypté et que nous testons toutes les possibilités pour pouvoir retrouver le message chiffré, nous allons au bout d'un moment forcément tomber sur un mot qui existe, mais cela peut-être n'importe lequel faisant la même taille. 

Pour pouvoir appliquer le chiffre de Vernam à un message secret avec une clef de mon choix, j'ai dû le coder en python, et voici à quoi cela ressemble :

![code python du chiffre de Vernam](./python/imagespy/B7codechiffrevernam.png)

Voici le résultat de l'application du chiffre de vernam sur le message secret "commissariat" et la clé "pateencroute" : 

![application du chiffre de vernam](./python/imagespy/B7applicationvernam.png)

Voici maintenant le code de decryptage du chiffre de Vernam, donc la procédure inversée en partant d'un message crypté et d'une clef, et voici quel est le résultat avec la clef "pateencroute" :

![code de decryptage du chiffre de vernam](./python/imagespy/B7codedecryptechiffrevernam.png)

On trouve donc bien le mot initial qui était "commissariat"

## B.8 : 

L'usage massif de la surveillance du net dans l'espoir d'empêcher des groupes illégaux de communiquer de façon discrète et secrète peut d'une part être très utile car de nombreuses personnes créent des canaux de discussions privés dans des buts hors la loi, mais d'un autre côté elle est assez problématique car ceux qui sont dans des attitudes totalement légales sont par la même occasion espionnés et sont alors exemptés de leur vie privée.

Comment mélanger la stéganographie et la cryptographie :

1. On passe la couleur rouge pour chaque pixel de l'image dans laquelle on veut cacher le texte à une valeur paire à l'aide de la fonction **rouge_pair**.

2. On cache le texte dans l'image grâce à la fonction **cacher_texte** tout en utilisant le code **chiffre_de_vernam** qui définit le texte lors de l'appel de la fonction.

3. On utilise la fonction **trouver_texte** pour trouver le message caché avant le décryptage.

4. On fait usage de la fonction **decryptage_chiffre_de_vernam** combinée à la fonction **trouver_texte** pour retrouver le message crypté.

Voici comment se présente le code python et ce que cela affiche :

![code python de la B8](./python/imagespy/B8code.png)

![affichage du code de la B8](./python/imagespy/B8affichage.png)

## FIN

Nous arrivons donc à la fin de la SAE Image. J'ai donc tout fait sauf la B9. 

Amaël Masérati