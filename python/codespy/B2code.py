from PIL import Image

i = Image.open("./python/imagespy/hall-mod_0.jpg")
sortie = i.copy()
# On crée une copie de l'image
for y in range(i.size[1]):
    for x in range(i.size[0]):
        # On parcourt l'image
        c = i.getpixel((x, y))
        # On change la valeur de x par son opposé pour l'effet miroir
        sortie.putpixel((-x,y),c)
# On sauvegarde l'image de sorte à la renommer
sortie.save("./python/imagespy/Imageout1.bmp")

