import B6code as B6 # On réutilise les fonctions du code B6
import B7code as B7 # On réutilise les fonctions du code B7

# Appel de la fonction rouge pair

B6.rouge_paire("./python/imagespy/hall-mod_0.bmp", "./python/imagespy/ImageB6.bmp")

B6.cacher_texte("./python/imagespy/ImageB6.bmp", B7.chiffre_de_vernam("commissariat", "pateencroute"), "./python/imagespy/Imageout_steg_1.bmp")

print("Le texte caché avant le décryptage est : ", B6.trouver_texte("./python/imagespy/Imageout_steg_1.bmp"))

print("Le message crypté était : ", B7.decrypter_chiffre_de_vernam(B6.trouver_texte("./python/imagespy/Imageout_steg_1.bmp"), "pateencroute"))

